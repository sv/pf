from dis_snek import Embed, EmbedAuthor, InteractionContext, Scale, Snake, slash_command, slash_option, OptionTypes, Color

class Polls(Scale):
    @slash_command(name="poll", description="ask a question")
    @slash_option(name="question", description="what you want to ask", opt_type=OptionTypes.STRING, required=True)
    async def poll(self, ctx: InteractionContext, question: str):
        em = Embed(title=question, author=EmbedAuthor(name=f'{ctx.author.display_name} asks...'), color=Color(0x7289da))
        msg = await ctx.send(embeds=[em])
        await msg.add_reaction("👍")
        await msg.add_reaction("🤏")
        await msg.add_reaction("👎")

    @slash_command(name="bigpoll", description="make a poll with custom options")
    @slash_option(name="question", description="what you want to ask", opt_type=OptionTypes.STRING, required=True)
    @slash_option(name="options", description="put your options here, seperate each one with a comma (eg: option1, option two, option iii)", opt_type=OptionTypes.STRING, required=True)
    async def bigpoll(self, ctx: InteractionContext, question: str, options: str):
        EMOJI = "🇦 🇧 🇨 🇩 🇪 🇫 🇬 🇭 🇮 🇯".split(" ")
        opt_list = [o.strip() for o in options.split(',')]
        desc = ""
        for i, o in enumerate(opt_list):
            desc += f"{EMOJI[i]} {o}\n"
        em = Embed(title=question, color=Color(0x7289da), description=desc, author=EmbedAuthor(name=f"{ctx.author.display_name} asks..."))
        msg = await ctx.send(embeds=[em])
        for i, _ in enumerate(opt_list):
            await msg.add_reaction(EMOJI[i])



def setup(bot: Snake):
    Polls(bot) 