from dis_snek import AutoArchiveDuration, Embed, Guild, Member, Snake, slash_command, InteractionContext, slash_option, OptionTypes, SlashCommandChoice, Intents, listen, Status, Activity, ActivityType
from os import getenv, stat
from dotenv import load_dotenv

load_dotenv()

bot = Snake(
    sync_interactions=True,
    intents=Intents.ALL,
    activity=Activity(name="things (/)", type=ActivityType.WATCHING)
)

@listen()
async def on_ready():
    print("bot ready!")


@slash_command(name="suggest", description="suggest something")
@slash_option(name="suggestion", description="your suggestion", required=True, opt_type=OptionTypes.STRING)
async def suggest(ctx: InteractionContext, suggestion: str):
    if ctx.guild_id != 733508936216477706: return await ctx.send("no", ephemeral=True) 
    gd = await bot.fetch_guild(733508936216477706)
    ch = await gd.fetch_channel(735619559318487123)
    if gd is None or ch is None: return await ctx.send("oopsie happened", ephemeral=True)
    em = Embed(title="Suggestion", description=suggestion)
    em.set_author(name=f"{str(ctx.author)} ({ctx.author.nick})", icon_url=ctx.author.avatar.url)
    msg = await ch.send(embeds=[em])
    await msg.add_reaction("👍")
    await msg.add_reaction("👎")
    await msg.create_thread(name="discuss", auto_archive_duration=AutoArchiveDuration.THREE_DAY)
    await ctx.send(f"[suggestion sent!]({msg.jump_url})", ephemeral=True)

@slash_command(name="g", description="good (morning/afternoon/night) someone")
@slash_option(
    name="type", 
    description="what type of thing (gm/ga/gn)", 
    required=True, 
    opt_type=OptionTypes.STRING,
    choices=[
        SlashCommandChoice(name="gm", value="gm"),
        SlashCommandChoice(name="ga", value="ga"),
        SlashCommandChoice(name="gn", value="gn")
    ]
)
@slash_option(name="who", description="who you want to say gm/ga/gn to (optional)", required=False, opt_type=OptionTypes.USER)
async def g(ctx: InteractionContext, type: str, who: Member = None):
    gtgs = {
        "gm": "good morning",
        "ga": "good afternoon",
        "gn": "good night",
    }
    if who is None:
        return await ctx.send(f"{ctx.author.mention} says {gtgs[type]}")
    else:
        return await ctx.send(f"{ctx.author.mention} says {gtgs[type]} to {who.user.mention}")

bot.grow_scale("scales.poll")
bot.grow_scale("dis_snek.ext.debug_scale")

bot.start(getenv("TOKEN"))